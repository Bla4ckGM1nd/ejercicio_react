# React JSX Ejercicios- SBG / SCMR

En este repositorio se presentara los varios ejercicios propuestos sobre React JSX ya resueltos. Los integrantes de este proyecto fueron Stephanie Blanquicett Garcia y Sayan Camilo Mosquera Román

___

## Funciones 
### JSX

![JSX](https://gitlab.com/Bla4ckGM1nd/ejercicio_react/-/blob/master/img/JSX-LOGO.png)

JSX es una extensión de JavaScript creada por Facebook para el uso con su librería React. Sirve de preprocesador (como Sass o Stylus a CSS) y transforma el código a JavaScript. De primeras te puede parecer que estás mezclando código HTML dentro de tus ficheros JavaScript, pero nada más lejos de la realidad.

#### Sintaxis basica


![Sintaxis - JSX](https://gitlab.com/Bla4ckGM1nd/ejercicio_react/-/blob/master/img/sintaxis-jsx.png)


### GIT

Git es una herramienta que realiza una función del control de versiones de código de forma distribuida, de la que destacamos varias características:

1. Es muy potente
2. Fue diseñada por Linus Torvalds
3. No depende de un repositorio central
4. Es software libre
5. Con ella podemos mantener un historial completo de versiones
Podemos movernos, como si tuviéramos un puntero en el tiempo, por todas las revisiones de código y desplazarnos una manera muy ágil.

#### Funcion basica
![GIT](https://github.com/Bla4ckGM1nd/TALLER_1_GIT/blob/master/assets/img/git_vida_mrr.png)

___

Espero haya sido de tu agrado esta pequeña guia!! Si tienes alguna pregunta, no dudes en contactarnos ;) !!

>> Correo: stephagarcia02@hotmail.com / camilomodquera@gmail.com